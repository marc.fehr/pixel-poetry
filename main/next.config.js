module.exports = {
  images: {
    domains: ['assets.vercel.com', 'lh3.googleusercontent.com', 'googleusercontent.com', 's.gravatar.com', 'gravatar.com', 'firebasestorage.googleapis.com']
  },
}
