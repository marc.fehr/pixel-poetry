import React, { createContext, useReducer } from 'react'
import Reducer from './reducer'

const initialState = {
  baseRef:
    process.env.NODE_ENV === 'production'
      ? 'https://www.pixelpoetry.dev/'
      : 'http://localhost:3000/',
  posts: [],
  error: null,
  form: {
    slug: '',
    error: '',
    description: '',
    lastPublished: '',
    images: [
      {
        id: 0,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 1,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 2,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 3,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 4,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 5,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 6,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 7,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
      {
        id: 8,
        // src: 'https://firebasestorage.googleapis.com/v0/b/pixelpoetry-2eb90.appspot.com/o/userUploads%2Fchilly-moth-7%2Ftile-1.jpg?alt=media&token=b0ea6bad-0804-4d59-bddc-bc555e024072',
        src: '',
      },
    ],
  },
}

const Store = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, initialState)
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  )
}

export const Context = createContext(initialState)

export default Store
