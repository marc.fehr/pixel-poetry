const Reducer = (state, action) => {
  switch (action.type) {
    case 'SET_IMG':
      const { src, id } = action.payload
      let newImageArray = []
      for (let i = 0; i < state.form.images.length; i++) {
        if (i === id) {
          newImageArray.push({
            src: src,
            id: id,
          })
        } else {
          newImageArray.push(state.form.images[i])
        }
      }

      return {
        ...state,
        form: {
          ...state.form,
          images: newImageArray,
        },
      }
    case 'SET_SLUG':
      return {
        ...state,
        form: {
          ...state.form,
          slug: action.payload,
        },
      }
    case 'SET_PUBLICATION':
      return {
        ...state,
        form: {
          ...state.form,
          lastPublished: action.payload,
        },
      }
    case 'SET_USER':
      return {
        ...state,
        form: {
          ...state.form,
          userId: action.payload,
        },
      }
    case 'SET_DESCRIPTION':
      return {
        ...state,
        form: {
          ...state.form,
          description: action.payload,
        },
      }
    case 'SET_POSTS':
      return {
        ...state,
        posts: action.payload,
      }
    case 'ADD_POST':
      return {
        ...state,
        posts: state.posts.concat(action.payload),
      }
    case 'REMOVE_POST':
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.payload),
      }
    case 'SET_ERROR':
      return {
        ...state,
        error: action.payload,
      }
    default:
      return state
  }
}

export default Reducer
