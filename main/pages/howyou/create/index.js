// import { useAppContext } from '@context/state'
import { withPageAuthRequired } from '@auth0/nextjs-auth0'

import WriteToCloudFirestore from '../../../components/cloudFirestore/write'
import LayoutComponent from '../../../components/ui/layout'
import ThumbnailPhoto from '../../../components/ui/thumbnail'
import FormStep from '../../../components/ui/formStep'

const CreateHowYou = (props) => {
  const { form } = useAppContext()
  const { images } = form

  const ThumbnailPhotos = images.map((el, i) => {
    return <ThumbnailPhoto key={`photo-${i}`} src={el.src} id={el.id} />
  })

  return (
    <LayoutComponent inline>
      <div className='max-w-3xl mx-auto'>
        <form>
          <FormStep
            title={'Slug'}
            description={'First, create your unique identifier...'}
          />

          <div>
            <label
              htmlFor='about'
              className='block text-sm font-medium text-gray-700'
            >
              About
            </label>
            <div className='mt-1'>
              <textarea
                id='about'
                name='about'
                rows='3'
                className='block w-full p-2 border-2 border-gray-300 rounded-md shadow-sm focus:ring-green-500 focus:border-green-500'
                placeholder='Description of this howyou.today quiz...'
              ></textarea>
            </div>
          </div>

          <div>
            <label className='block text-sm font-medium text-gray-700'>
              Photos
            </label>
            <div className={'grid grid-cols-3 gap-2 justify-evenly'}>
              {ThumbnailPhotos}
            </div>
          </div>

          <div className='px-4 py-3 text-center'>
            <WriteToCloudFirestore
              data={{
                slug: "asdf",
                description: "asdf",
                userId: props.user.sub,
              }}
            >
              <span>Save to Firestore</span>
            </WriteToCloudFirestore>
          </div>
        </form>
      </div>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYou
