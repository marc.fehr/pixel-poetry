import { useState, useEffect, useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'
import { ErrorMessageComponentInline } from '@components/ui/errorMessage'
import { SuccessMessageComponentInline } from '@components/ui/successMessage'

const CreateHowYouPublish = (props) => {
  const [state, dispatch] = useContext(Context)
  const { sub } = props.user

  const [isPublishable, setIsPublishable] = useState(true)
  
  useEffect(() => {
    dispatch({
      type: 'SET_USER',
      payload: sub,
    })
  }, [props.user])

  useEffect(() => {
    if (
      !state.form.slug ||
      !state.form.description ||
      !state.form.userId ||
      state.form.images.filter((el) => el.src === '').length > 0
    ) {
      setIsPublishable(false)
    }
  }, [state.form])

  return (
    <LayoutComponent noFooter>
      <FormStep
        title={'How does this look?'}
        description={'Hit publish and start sharing.'}
        nextStep={null}
        publishButton={isPublishable}
        previousStep={'/howyou/create/images'}
      >
        <div className='flex flex-row w-full'>
          <ul>
            <li className={'py-1'}>Created by: {props.user.name}</li>
            <li className={'py-1'}>
              Slug:{' '}
              {state.form.slug === '' ? (
                <ErrorMessageComponentInline>
                  Missing
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.form.slug}
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'py-1'}>
              Description:{' '}
              {state.form.description === '' ? (
                <ErrorMessageComponentInline>
                  Missing
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.form.description}
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'py-1'}>
              Images:{' '}
              {state.form.images.filter((el) => el.src === '').length > 0 ? (
                <ErrorMessageComponentInline>
                  {state.form.images.filter((el) => el.src !== '').length} of{' '}
                  {state.form.images.length} images uploaded
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.form.images.filter((el) => el.src !== '').length} of{' '}
                  {state.form.images.length} images uploaded
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'py-1'}>
              Share link:{' '}
              <strong>{`https://howyou.today/${state.form.slug}`}</strong>
              {state.slug === '' ? (
                <ErrorMessageComponentInline>
                  Missing
                </ErrorMessageComponentInline>
              ) : (
                <>
                  /
                  <SuccessMessageComponentInline>
                    {state.slug}
                  </SuccessMessageComponentInline>
                </>
              )}
            </li>
            <li className={'py-1'}>
              Last published:{' '}
              {state.form.lastPublished === '' ? (
                <ErrorMessageComponentInline>
                  Never
                </ErrorMessageComponentInline>
              ) : (
                <>
                  <SuccessMessageComponentInline>
                    {state.form.lastPublished}
                  </SuccessMessageComponentInline>
                </>
              )}
            </li>
          </ul>
        </div>
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouPublish
