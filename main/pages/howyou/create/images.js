import { useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'
import ThumbnailPhoto from '@components/ui/thumbnail'
import ErrorMessageComponent from '@components/ui/errorMessage'

const CreateHowYouImages = (props) => {
  const [state, dispatch] = useContext(Context)

  const ThumbnailPhotos = state.form.images?.map((el, i) => {
    return <ThumbnailPhoto key={`photo-${i}`} src={el.src} id={el.id} />
  })

  return (
    <LayoutComponent noFooter>
      <FormStep
        title={'Upload your moods'}
        description={'You need to add a total of nine images.'}
        nextStep={'/howyou/create/publish'}
        previousStep={'/howyou/create/description'}
      >
        <div className='flex flex-row w-full'>{ThumbnailPhotos}</div>
        {state.form.slug === '' ||
          (!state.form.slug && (
            <ErrorMessageComponent className={'mt-3'}>
              Please define a slug before you upload images.
            </ErrorMessageComponent>
          ))}
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouImages
