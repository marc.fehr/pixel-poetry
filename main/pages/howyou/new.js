import { useState } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { useForm } from 'react-hook-form'

import WriteToCloudFirestore from '../../components/cloudFirestore/write'
import LayoutComponent from '../../components/ui/layout'
import ThumbnailPhoto from '../../components/ui/thumbnail'

const NewHowYou = (props) => {
  const [images, setImages] = useState([
    {
      id: 0,
      src: '',
    },
    {
      id: 1,
      src: '',
    },
    {
      id: 2,
      src: '',
    },
    {
      id: 3,
      src: '',
    },
    {
      id: 4,
      src: '',
    },
    {
      id: 5,
      src: '',
    },
    {
      id: 6,
      src: '',
    },
    {
      id: 7,
      src: '',
    },
    {
      id: 8,
      src: '',
    },
  ])

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
  } = useForm()
  const onSubmit = (data) => console.log(data)

  const defaultValues = {
    slug: props.uid,
  }

  const ThumbnailPhotos = images.map((el, i) => {
    return <ThumbnailPhoto key={`photo-${i}`} src={el.src} id={el.id} />
  })

  const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

  // console.log(props)

  return (
    <LayoutComponent inline>
      <div className='max-w-3xl mx-auto'>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div>
            <div className='px-4 py-5 space-y-6 bg-white sm:p-6'>
              <div className='grid grid-cols-3'>
                <div className='col-span-3'>
                  <label
                    htmlFor='company_website'
                    className='block text-sm font-medium text-gray-700'
                  >
                    Create a share link
                  </label>
                  <div className='flex flex-1 mt-1 rounded-md shadow'>
                    <span className='inline-flex items-center pl-2 pr-1 text-2xl text-gray-500 border-2 border-r-0 border-gray-300 rounded-l-md bg-gray-50'>
                      howyou.today/
                    </span>
                    <input
                      defaultValue={defaultValues.slug}
                      type='text'
                      {...register('slug', {
                        required: true,
                        // validate: async (value) => {
                        //   await sleep(1000)
                        //   return value === 'bill'
                        // },
                      })}
                      id='slug'
                      className='block w-full pl-0 text-2xl font-bold text-green-500 border-2 border-l-0 border-gray-300 rounded-none focus:ring-green-500 focus:border-green-500 rounded-r-md bg-gray-50'
                      placeholder='angry-sheep-44'
                    />
                    {/* {errors.slug && <p>Please enter a valid slug.</p>} */}
                  </div>
                </div>
              </div>

              <div>
                <label
                  htmlFor='about'
                  className='block text-sm font-medium text-gray-700'
                >
                  About
                </label>
                <div className='mt-1'>
                  <textarea
                    id='about'
                    name='about'
                    rows='3'
                    className='block w-full p-2 border-2 border-gray-300 rounded-md shadow-sm focus:ring-green-500 focus:border-green-500'
                    placeholder='Description of this howyou.today quiz...'
                    {...register('description')}
                  ></textarea>
                </div>
              </div>

              <div>
                <label className='block text-sm font-medium text-gray-700'>
                  Photos
                </label>
                <div className={'grid grid-cols-3 gap-2 justify-evenly'}>
                  {ThumbnailPhotos}
                </div>
              </div>
            </div>
            <div className='px-4 py-3 text-center'>
              <WriteToCloudFirestore
                data={{
                  slug: getValues('slug') || props.uid,
                  description: getValues('description'),
                  userId: props.user.sub,
                }}
              >
                <span>Save to Firestore</span>
              </WriteToCloudFirestore>
            </div>
          </div>
        </form>
      </div>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired({
  // returnTo: '/foo',
  async getServerSideProps(ctx) {
    const baseRef =
      process.env.NODE_ENV === 'production'
        ? process.env.BASE_URL_PROD
        : process.env.BASE_URL_DEV
    const res = await fetch(`${baseRef}api/uid/create`)
    const data = await res.json()

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        uid: data.uid,
      },
    }
  },
})

export default NewHowYou
