import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link'

import { useUser } from '@auth0/nextjs-auth0'

import LayoutComponent from '../components/ui/layout'
import ButtonComponent from '../components/ui/button'

export default function Home() {
  const { user, error, isLoading } = useUser()

  if (isLoading)
    return (
      <LayoutComponent>
        <Image
          src={'/images/logo-square@3x.png'}
          width={100}
          height={100}
          alt={'PixelPoetry logo'}
        />
        <p>Loading...</p>
      </LayoutComponent>
    )
  if (error) return <div>{error.message}</div>

  return (
    <LayoutComponent>
      <Head>
        <title>PixelPoetry</title>
        <meta name='description' content='Generated by create next app' />
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <section className={'flex justify-center items-center flex-col'}>
        <h1>Welcome to PixelPoetry</h1>
        {user ? (
          <>
            <ButtonComponent
              href={'/howyou/create/slug'}
              className={'bg-green-400 hover:bg-green-200 focus:outline-none'}
            >
              <span className={'mr-2'} role={'img'} aria-label={'Sheep'}>
                🐑
              </span>
              <span>Create a new HowYou quiz</span>
            </ButtonComponent>
          </>
        ) : (
          <p>
            Please <Link href={'/api/auth/login'}>login</Link> to use this site.
          </p>
        )}
      </section>
    </LayoutComponent>
  )
}
