import { UserProvider } from '@auth0/nextjs-auth0'
// import { AppWrapper } from '../context/state'
import Store from '@context/store'

import firebase from '../firebase/initFirebase'

import '../styles/tailwind.css'
import '../styles/globals.scss'

firebase()

function PixelPoetryApp({ Component, pageProps }) {
  const { user } = pageProps

  return (
    <Store>
      <UserProvider user={user}>
        <Component {...pageProps} />
      </UserProvider>
    </Store>
  )
}

export default PixelPoetryApp
