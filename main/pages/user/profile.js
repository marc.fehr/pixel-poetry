import Image from 'next/image'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import WriteToCloudFirestore from '../../components/cloudFirestore/write'
import ReadFromCloudFirestore from '../../components/cloudFirestore/read'
import Counter from '../../components/realtimeDatabase/counter'
import UploadFile from '../../components/storage/uploadFile'

import LayoutComponent from '../../components/ui/layout'

export default function Profile(props) {
  console.log(props)

  return (
    <LayoutComponent>
      <h1 className={'mb-4'}>Hello, {props.user.name}</h1>
      {props.user.picture ? (
        <Image
          src={props.user.picture}
          width={150}
          height={150}
          className={'rounded-lg'}
        />
      ) : (
        <div
          style={{ width: '150px', height: '150px' }}
          className={'rounded-lg bg-gray-200 flex justify-center items-center'}
        >
          <p className={'text-gray-500'}>Photo missing</p>
        </div>
      )}
      <div className={'m-2 mt-4'}>
        <WriteToCloudFirestore />
      </div>
      <div className={'m-2'}>
        <ReadFromCloudFirestore />
      </div>
      <div className={'m-2'}>
        <Counter id={props.user.sub} />
      </div>
      <div className={'m-2'}>
        <UploadFile />
      </div>
    </LayoutComponent>
  )
}

// export const getServerSideProps = withPageAuthRequired()

// You can optionally pass your own `getServerSideProps` function into
// `withPageAuthRequired` and the props will be merged with the `user` prop

export const getServerSideProps = withPageAuthRequired({
  // returnTo: '/foo',
  async getServerSideProps(ctx) {
    const res = await fetch(`https://jsonplaceholder.typicode.com/todos/1`)
    const data = await res.json()

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        ...data,
      },
    }
  },
})
