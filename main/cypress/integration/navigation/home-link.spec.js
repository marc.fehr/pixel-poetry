describe('Nav Menus', () => {
  // For desktop view
  context('720p resolution', () => {
    beforeEach(() => {
      cy.viewport('macbook-13')
    })
    describe('When you visit home', () => {
      it('Should visit about page', () => {
        cy.visit('/about')
      })
      describe('nav', () => {
        it('Should navigate to Homepage', () => {
          cy.get('nav').find('a:first-child').click()
          cy.url().should('include', '/')
        })
      })
    })
  })
  context('iphone-5 resolution', () => {
    beforeEach(() => {
      cy.viewport('iphone-6')
    })
    describe('When you visit home', () => {
      it('Should visit about page', () => {
        cy.visit('/about')
      })
      describe('nav', () => {
        it('Should navigate to About page', () => {
          cy.get('nav').find('a:first-child').click()
          cy.url().should('include', '/')
        })
      })
    })
  })
})
