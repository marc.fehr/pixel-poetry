import firebase from 'firebase/app'
import 'firebase/firestore'

import { SimpleButton } from '../ui/button'

const ReadFromCloudFirestore = () => {
  const readData = () => {
    try {
      firebase
        .firestore()
        .collection('myCollection')
        .doc('myDocument')
        .onSnapshot(function (doc) {
          console.log(doc.data())
        })
      alert('Data was fetched from Cloud Firestore!')
    } catch (error) {
      console.log(error)
      alert(error)
    }
  }

  return (
    <SimpleButton onClick={readData}>
      <span>Read data from Cloud Firestore</span>
    </SimpleButton>
  )
}

export default ReadFromCloudFirestore
