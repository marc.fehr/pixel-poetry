import { useContext } from 'react'

import firebase from 'firebase/app'
import 'firebase/firestore'

import { Context } from '@context/store'

import ButtonComponent, { SimpleButton } from '../ui/button'

const WriteToCloudFirestore = (props) => {
  const [state, dispatch] = useContext(Context)

  const sendData = () => {
    try {
      firebase
        .firestore()
        .collection('howyou')
        .add({
          slug: state.form.slug,
          userId: state.form.userId,
          description: state.form.description,
          images: state.form.images,
          lastPublished: firebase.firestore.Timestamp.fromDate(new Date()),
        })
        .then((docRef) => {
          console.log('Data was successfully sent to cloud firestore!')
          docRef
            .get()
            .then((doc) => {
              if (doc.exists) {
                console.log('Document data:', doc.data())
                dispatch({
                  type: 'SET_PUBLICATION',
                  payload: new Date(
                    (doc.data().lastPublished.seconds +
                      doc.data().lastPublished.nanoseconds * 10 ** -9) *
                      1000
                  ).toString(),
                })
              } else {
                console.log('No such document!')
              }
            })
            .catch((error) => {
              console.log('Error getting document:', error)
            })
        })
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <>
      {state.form.lastPublished !== '' ? (
        <ButtonComponent rel={'noopener noreferrer'} target={'_blank'} className={'bg-green-300 hover:bg-green-200'} href={`https://www.howyou.today/${state.form.slug}`}>
          Visit now
        </ButtonComponent>
      ) : (
        <SimpleButton
          onClick={sendData}
          className={`bg-green-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-green-200 focus:outline-none ${
            state.form.lastPublished === '' ? 'opacity-100' : 'opacity-50'
          }`}
        >
          {props.children || <span>Publish</span>}
        </SimpleButton>
      )}
    </>
  )
}

export default WriteToCloudFirestore
