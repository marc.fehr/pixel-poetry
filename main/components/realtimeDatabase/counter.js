import { useState, useEffect } from 'react'

import firebase from 'firebase/app'
import 'firebase/database'

import { SimpleButton } from '../../components/ui/button'

const Counter = ({ id }) => {
  const cleanId = encodeURIComponent(id.replaceAll('.',''))
  
  const [count, setCount] = useState('')
  useEffect(() => {
    const onCountIncrease = (count) => setCount(count.val())

    const fetchData = async () => {
      firebase.database().ref('counts').child(cleanId).on('value', onCountIncrease)
    }

    fetchData()

    return () => {
      firebase.database().ref('counts').child(cleanId).off('value', onCountIncrease)
    }

  }, [cleanId])

  const increaseCount = async () => {
    const registerCount = () =>
      fetch(`/api/counter/incrementCount?id=${encodeURIComponent(cleanId)}`)
    registerCount()
  }

  return (
    <SimpleButton onClick={increaseCount}>
      Increase Count {count || '---'}
    </SimpleButton>
  )
}

export default Counter
