import Image from 'next/image'
import Link from 'next/link'

import { useUser } from '@auth0/nextjs-auth0'

import ButtonComponent from './button'

const NavigationComponent = (props) => {
  const { user, error, isLoading } = useUser()

  return (
    <nav className='fixed top-0 left-0 flex flex-wrap items-center w-full px-2 py-1 bg-gray-100 shadow-lg flex-start'>
      <Link href={'/'}>
        <a className={'simple'}>
          <Image
            src={'/images/logo-square@3x.png'}
            className={'cursor-pointer rounded-lg'}
            width={50}
            height={50}
            alt={'Logo PixelPoetry'}
          />
        </a>
      </Link>
      <div className={'mr-3'} />
      {!user && (
        <div className={'flex flex-row justify-between flex-1'}>
          <div className={'flex justify-start items-center'}>
            {props.children}
          </div>
          <div className={'flex justify-end items-center'}>
            <ButtonComponent
              href='/api/auth/login'
              className={'mr-2 bg-green-600 hover:bg-green-400 text-white'}
            >
              <span>Login</span>
            </ButtonComponent>
          </div>
        </div>
      )}
      {user && (
        <div className={'flex flex-row justify-between flex-1'}>
          <div className={'flex justify-start items-center'}>
            {props.children}
          </div>
          <div className={'flex justify-end items-center'}>
            <ButtonComponent
              href='/api/auth/logout'
              className={'mr-2 bg-red-600 hover:bg-red-400 text-white'}
            >
              <span>Logout</span>
            </ButtonComponent>
            <Link href='/user/profile'>
              <a className={'simple'}>
                <Image
                  className={
                    'rounded-full w-full h-full top-0 left-0 m-0 p-0rounded-full border-4 border-green-400 cursor-pointer hover:opacity-70'
                  }
                  src={user.picture}
                  width={50}
                  height={50}
                  alt={'Profile picture'}
                />
              </a>
            </Link>
          </div>
        </div>
      )}
    </nav>
  )
}

export default NavigationComponent
