import NavigationComponent from './navigation'
import FooterComponent from './footer'

const LayoutComponent = ({ children, className, noFooter, inline }) => {
  return (
    <>
      <NavigationComponent />
      <main
        className={`${
          inline ? 'h-auto mt-20 pb-20' : 'h-screen min-h-screen-ios'
        } flex justify-center items-center flex-col ${className}`}
      >
        {children}
      </main>
      {!noFooter && <FooterComponent />}
    </>
  )
}

export default LayoutComponent
