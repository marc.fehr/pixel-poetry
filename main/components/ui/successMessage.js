const SuccessMessageComponent = (props) => {
  return (
    <p className={`py-1 px-2 bg-green-400 text-white flex flex-1 w-full rounded ${props.className}`}>
      {props.children}
    </p>
  )
}

export const SuccessMessageComponentInline = (props) => {
  return (
    <p className={`py-1 px-2 bg-green-400 text-white inline rounded ${props.className}`}>
      {props.children}
    </p>
  )
}

export default SuccessMessageComponent
