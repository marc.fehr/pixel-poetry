import Link from 'next/link'

const ButtonComponent = ({ children, href, className, target, rel }) => {
  return (
    <Link href={href}>
      <a target={target || '_self'} rel={rel || ''} className={`py-1 px-3 bg-gray-300 hover:bg-gray-200 ${className} simple`}>
          {children}
      </a>
    </Link>
  )
}

export const SimpleButton = ({ children, onClick, className }) => {
  return (
    <button
      onClick={onClick}
      className={`py-1 px-3 bg-gray-300 hover:bg-gray-200 ${className}`}
    >
      {children}
    </button>
  )
}

export default ButtonComponent
