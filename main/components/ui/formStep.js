import { useContext } from 'react'

import Link from 'next/link'
import { Context } from '@context/store'

import WriteToCloudFirestore from '@components/cloudFirestore/write'

const FormStep = (props) => {
  const [state, dispatch] = useContext(Context)

  const Steps = () => {
    return (
      <>
        <p className={'mb-1'}>Progress:</p>
        <div className={'mb-4 grid grid-cols-4 items-center'}>
          <Link href={'/howyou/create/slug'}>
            <a className={'simple'}>
              <div
                className={` shadow-sm rounded-l-full bg-gray-200 py-1 px-2 ${
                  state.form.slug === '' ? 'bg-gray-100' : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Set slug</p>
              </div>
            </a>
          </Link>
          <Link href={'/howyou/create/description'}>
            <a className={'simple'}>
              <div
                className={`shadow-sm bg-gray-200 py-1 px-2 ${
                  !state.form.description ? 'bg-gray-100' : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Description</p>
              </div>
            </a>
          </Link>
          <Link href={'/howyou/create/images'}>
            <a className={'simple'}>
              <div
                className={`shadow-sm bg-gray-200 py-1 px-2 ${
                  state.form.images?.filter((el) => el.src === '').length > 0 ||
                  !state.form.images
                    ? 'bg-gray-100'
                    : 'bg-green-300'
                } `}
              >
                <p className={'text-center'}>Images</p>
              </div>
            </a>
          </Link>
          <Link href={'/howyou/create/publish'}>
            <a className={'simple'}>
              <div
                className={`rounded-r-full shadow-sm bg-gray-200 py-1 px-2 ${
                  !state.form.lastPublished ? 'bg-gray-100' : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Publish</p>
              </div>
            </a>
          </Link>
        </div>
      </>
    )
  }

  const Controls = () => {
    return (
      <div className={'flex flex-row justify-between'}>
        <Link href={props.previousStep || '#'}>
          <a
            className={`bg-gray-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-gray-200 ${
              props.previousStep
                ? 'pointer-events-auto'
                : 'pointer-events-none opacity-0'
            }`}
          >
            <button>Back</button>
          </a>
        </Link>
        {!props.publishButton ? (
          <Link href={props.nextStep || '#'}>
            <a
              className={`bg-gray-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-gray-200 ${
                props.nextStep
                  ? 'pointer-events-auto'
                  : 'pointer-events-none opacity-0'
              }`}
            >
              <button>Next</button>
            </a>
          </Link>
        ) : (
          <div
            className={
              'bg-green-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-green-200'
            }
          >
            <WriteToCloudFirestore />
          </div>
        )}
      </div>
    )
  }

  return (
    <>
      <Steps />
      <div
        className={
          'px-3 py-2 rounded-none border-none sm:border-gray-300 shadow-lg border-2 sm:rounded-xl flex w-full h-full flex-col sm:w-3/4 sm:h-1/3 md:w-1/2 lg:h-1/3 lg:w-1/2 justify-betweeen'
        }
      >
        <div className={'flex-1'}>
          <div className={'mb-3 rounded-lg'}>
            <h1>{props.title}</h1>
            <h2>{props.description}</h2>
          </div>
          <div className={'flex flex-col items-start justify-center'}>
            {props.children}
          </div>
          {props.error && (
            <p className={'bg-red-400 text-white rounded-lg text-lg py-1 px-2'}>
              {props.error}
            </p>
          )}
        </div>
        <div>
          <Controls />
        </div>
      </div>
    </>
  )
}

export default FormStep
