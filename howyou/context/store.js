import React, { createContext, useReducer } from 'react'
import Reducer from './reducer'

const initialState = {
  baseRef:
    process.env.NODE_ENV === 'production'
      ? 'https://www.howyou.today/'
      : 'http://localhost:3000/',
  images: [],
  userVote: '-1',
  form: {
    error: '',
    description: '',
    lastPublished: '',
    images: [
      {
        id: 0,
        src: '',
      },
      {
        id: 1,
        src: '',
      },
      {
        id: 2,
        src: '',
      },
      {
        id: 3,
        src: '',
      },
      {
        id: 4,
        src: '',
      },
      {
        id: 5,
        src: '',
      },
      {
        id: 6,
        src: '',
      },
      {
        id: 7,
        src: '',
      },
      {
        id: 8,
        src: '',
      },
    ],
  },
  slug: null,
  error: null,
}

const Store = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, initialState)
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  )
}

export const Context = createContext(initialState)

export default Store
