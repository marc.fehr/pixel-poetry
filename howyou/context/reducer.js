const Reducer = (state, action) => {
  switch (action.type) {
    case 'SET_SLUG':
      return {
        ...state,
        slug: action.payload,
      }
    case 'SET_IMAGES':
      return {
        ...state,
        images: action.payload,
      }
    case 'SET_SESSION':
      return {
        ...state,
        sessionId: action.payload,
      }
    case 'SET_USERVOTE':
      return {
        ...state,
        userVote: action.payload,
      }
    case 'SET_COLOR':
      return {
        ...state,
        color: action.payload,
      }
    case 'SET_IMG_TEMPLATE':
      console.log(action.payload)
      return {
        ...state,
        form: {
          ...state.form,
          images: action.payload,
        },
      }
    case 'SET_IMG':
      const { src, id } = action.payload
      let newImageArray = []
      for (let i = 0; i < state.form.images.length; i++) {
        if (i === id) {
          newImageArray.push({
            src: src,
            id: id,
          })
        } else {
          newImageArray.push(state.form.images[i])
        }
      }

      return {
        ...state,
        form: {
          ...state.form,
          images: newImageArray,
        },
      }
    case 'SET_PUBLICATION':
      return {
        ...state,
        // slug: '',
        form: {
          ...state.form,
          lastPublished: action.payload,
        },
      }
    case 'SET_USER':
      return {
        ...state,
        form: {
          ...state.form,
          userId: action.payload,
        },
      }
    case 'SET_DESCRIPTION':
      return {
        ...state,
        form: {
          ...state.form,
          description: action.payload,
        },
      }
    case 'SET_ERROR':
      return {
        ...state,
        error: action.payload,
      }
    case 'RESET_ALL':
      return {
        ...state,
        slug: null,
        form: {
          ...state.form,
          error: '',
          description: '',
          lastPublished: '',
        },
        error: null,
      }
    default:
      return state
  }
}

export default Reducer
