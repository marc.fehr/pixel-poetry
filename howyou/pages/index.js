import { useContext } from 'react'

import { Context } from '@context/store'
import { useUser } from '@auth0/nextjs-auth0'

import Head from 'next/head'
import Link from 'next/link'

import LayoutComponent from '@components/ui/layout'
import ButtonComponent from '@components/ui/button'

export default function Home() {
  const [state, dispatch] = useContext(Context)
  const { user, error, isLoading } = useUser()

  const changeHandler = (e) => {
    const val = e.currentTarget.value

    if (val) {
      dispatch({
        type: 'SET_SLUG',
        payload: val,
      })
    } else {
      dispatch({
        type: 'SET_SLUG',
        payload: '',
      })
    }
  }

  return (
    <LayoutComponent clean>
      <Head>
        <title>HowYou?</title>
        <meta
          name='description'
          content='Generate your own mood indicator scale and share it with your colleagues.'
        />
        <link rel='icon' href='/favicon.ico' />
      </Head>
      <section className={'flex justify-center items-center flex-col'}>
        <div className={'max-w-xl'}>
          <h1>Welcome to HowYou.today</h1>
          <p>
            You crave some random sheep in your life? Bless you. This is the
            place. Check this example below or create your own random "Sheep
            Scale" mood indicator.
          </p>
          <div className='flex flex-row flex-1 w-full mt-2'>
            <span className='inline-flex pl-2 pr-1 text-lg text-gray-500 border-2 border-r-0 border-gray-300 sm:text-2xl rounded-l-md bg-gray-50'>
              howyou.today/
            </span>
            <span
              id='slug-example'
              className='block w-full pl-0 pr-2 text-lg font-bold text-green-500 border-2 border-l-0 border-gray-300 rounded-none sm:text-2xl focus:ring-green-500 focus:border-green-500 rounded-r-md bg-gray-50 focus:outline-none'
            >
              curvy-chicken-66
            </span>
          </div>
          <div className={'flex items-center justify-center flex-col'}>
            <ButtonComponent
              className={'mt-4 text-xl focus:outline-none'}
              href={`/curvy-chicken-66`}
            >
              Check the example
            </ButtonComponent>
            {user ? (
              <>
                <p className={'my-2'}>– OR –</p>
                <ButtonComponent
                  href={'/create/slug'}
                  className={
                    'bg-green-400 hover:bg-green-200 focus:outline-none text-xl'
                  }
                >
                  <span className={'mr-2'} role={'img'} aria-label={'Sheep'}>
                    🐑
                  </span>
                  <span>Create your own</span>
                </ButtonComponent>
              </>
            ) : (
              <p className={'mt-4 text-xl'}>
                Please <Link href={'/api/auth/login'}>login</Link> to create a
                new HowYou.today quiz.
              </p>
            )}
          </div>
        </div>
      </section>
    </LayoutComponent>
  )
}
