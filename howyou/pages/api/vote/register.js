import firebase from 'firebase/app'
import 'firebase/database'

import initFirebase from '/firebase/initFirebase'
initFirebase()

const registerVote = async (req, res) => {
  const { id, slug, sessionId } = req.query
  const ref = firebase.database().ref(`votes/${slug}`).child(sessionId)
  const { snapshot } = await ref.transaction((res) => {
    if (res === null) {
      return id
    }

    return res
  })

  return res.status(200).json({
    userVote: snapshot.val(),
  })
}

export default registerVote
