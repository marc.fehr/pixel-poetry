import firebase from 'firebase/app'
import 'firebase/database'

import initFirebase from '/firebase/initFirebase'
initFirebase()

const fetchVotes = async (req, res) => {
  console.log(req.query)

  const { slug } = req.query
  const ref = firebase
    .database()
    .ref(`votes/${slug}`)
    .on(
      'value',
      (snapshot) => {
        console.log(snapshot.val())
        return res.json()
      },
      (errorObject) => {
        console.log('The read failed: ' + errorObject.name)
        res.status(400).json({
          message: 'Error fetching data',
        })
      }
    )
    res.status(200).json(ref)
}

export default fetchVotes
