import firebase from 'firebase/app'
import 'firebase/firestore'

import initFirebase from '/firebase/initFirebase'
initFirebase()

const fetchData = async (req, res) => {
  const { slug } = req.query

  try {
    firebase
      .firestore()
      .collection('howyou')
      .where('slug', '==', slug)
      .get()
      .then((querySnapshot) => {
        console.log(querySnapshot.size + ' entry (or entries) found')
        if (querySnapshot.size === 1) {
          console.log(`Slug ${slug} found and is unique`)
          const data = querySnapshot.docs[0].data()
          res.status(200).json(data)
        } else {
          console.log(`Slug ${slug} was either not found or is not unique`)
        }
      })
  } catch (error) {
    console.log(error)
    res.status(400).json({
      message: "Error while fetching data"
    })
  }
}

export default fetchData
