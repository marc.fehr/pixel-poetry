const retry = require('async-await-retry')

import firebase from 'firebase/app'
import 'firebase/firestore'

import initFirebase from '/firebase/initFirebase'
initFirebase()

var hri = require('human-readable-ids').hri

const createUID = async (req, res) => {
  let currentUID = hri.random()
  let verifiedUID = undefined

  try {
    const res = await retry(
      async () => {
        return new Promise((resolve, reject) => {
          firebase
            .firestore()
            .collection('howyou')
            .where('slug', '==', currentUID)
            .get()
            .then((querySnapshot) => {
              if (querySnapshot.size > 0) {
                console.log(`UID ${currentUID} is taken!`)
                reject('Taken')
              } else {
                console.log(`UID ${currentUID} is available!`)
                verifiedUID = currentUID
                resolve('UID is valid and unique.')
              }
            })
        })
      },
      null,
      {
        retriesMax: 5,
        interval: 100,
        exponential: true,
        factor: 3,
        jitter: 100,
      }
    )
    console.log(res)
  } catch (err) {
    console.log('The function execution failed !')
  }

  // 3 Return valid uid
  return res.status(200).json({
    uid: verifiedUID,
  })
}

export default createUID
