import { useEffect, useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'

import { CopyToClipboard } from 'react-copy-to-clipboard'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'
import RefreshIcon from '@components/icons/refresh'
import ClipboardIcon from '@components/icons/clipboard'

const CreateHowYouSlug = (props) => {
  const [state, dispatch] = useContext(Context)

  const fetchSlug = async () => {
    const res = await fetch(`${state.baseRef}api/uid/create`)
    const data = await res.json()

    if (!data) {
      dispatch({
        type: 'SET_SLUG',
        payload: [''],
      })
    } else {
      dispatch({
        type: 'SET_SLUG',
        payload: data.uid,
      })
    }
  }

  useEffect(() => {
    if (state.slug) {
      return
    }
    fetchSlug()
  }, [])

  return (
    <LayoutComponent noFooter clean inline>
      <FormStep
        title={'Slug'}
        description={'First, create your unique identifier.'}
        nextStep={'/create/description'}
        previousStep={null}
      >
        <div className='flex flex-row flex-1 w-full mt-2'>
          <span className='inline-flex pl-2 pr-0 text-base text-gray-500 border-2 border-r-0 border-gray-300 sm:text-2xl rounded-l-md bg-gray-50'>
            howyou.today/
          </span>
          <span
            id='slug'
            className='block w-full pl-0 text-base font-bold text-green-500 border-2 border-l-0 border-gray-300 rounded-none sm:text-2xl focus:ring-green-500 focus:border-green-500 rounded-r-md bg-gray-50'
            placeholder='angry-sheep-44'
          >
            {state.slug}
          </span>
        </div>
        <div className={'mt-2'}>
          <button
            onClick={fetchSlug}
            className={
              'my-2 text-sm sm:text-base py-1 px-2 bg-green-300 rounded-full hover:bg-green-200 inline-flex justify-center items-center focus:outline-none'
            }
          >
            <span className={'w-6 inline-block mr-1'}>
              <RefreshIcon />
            </span>
            Get another URL
          </button>
          <CopyToClipboard text={`${state.baseRef}${state.slug}`}>
            <button
              className={
                'my-2 text-sm sm:text-base sm:ml-2 py-1 px-2 bg-gray-300 rounded-full hover:bg-blue-200 inline-flex justify-center items-center focus:outline-none'
              }
            >
              <span className={'w-6 inline-block mr-1'}>
                <ClipboardIcon />
              </span>
              Copy to clipboard
            </button>
          </CopyToClipboard>
        </div>
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouSlug
