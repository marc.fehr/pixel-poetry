import { useState, useEffect, useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'
import moment from 'moment'
import { CopyToClipboard } from 'react-copy-to-clipboard'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'
import { ErrorMessageComponentInline } from '@components/ui/errorMessage'
import { SuccessMessageComponentInline } from '@components/ui/successMessage'

const CreateHowYouPublish = (props) => {
  const [state, dispatch] = useContext(Context)
  const { sub } = props.user

  console.log(state)

  const [isPublishable, setIsPublishable] = useState(true)

  useEffect(() => {
    dispatch({
      type: 'SET_USER',
      payload: sub,
    })
  }, [props.user])

  useEffect(() => {
    if (
      !state.slug &&
      !state.form.description &&
      !state.form.userId &&
      state.form.images.filter((el) => el.src === '').length > 0 &&
      state.form.lastPublished === ''
    ) {
      setIsPublishable(false)
    }
  }, [state])

  return (
    <LayoutComponent noFooter clean inline>
      <FormStep
        title={'How does this look?'}
        description={'Hit publish and start sharing.'}
        nextStep={null}
        publishButton={isPublishable}
        previousStep={'/create/images'}
      >
        <div className='flex flex-row w-full mb-4 text-sm'>
          <ul>
            <li className={'pb-2'}>Created by: {props.user.name}</li>
            <li className={'pb-2'}>
              Slug: <br />
              {!state.slug ? (
                <ErrorMessageComponentInline>
                  Missing
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.slug}
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'pb-2'}>
              Description: <br />
              {state.form.description === '' ? (
                <ErrorMessageComponentInline>
                  Missing
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.form.description}
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'pb-2'}>
              Images: <br />
              {state.form.images.filter((el) => el.src === '').length > 0 ? (
                <ErrorMessageComponentInline>
                  {state.form.images.filter((el) => el.src !== '').length} of{' '}
                  {state.form.images.length} images uploaded
                </ErrorMessageComponentInline>
              ) : (
                <SuccessMessageComponentInline>
                  {state.form.images.filter((el) => el.src !== '').length} of{' '}
                  {state.form.images.length} images uploaded
                </SuccessMessageComponentInline>
              )}
            </li>
            <li className={'pb-2'}>
              Date published:{' '}<br />
              {state.form.lastPublished === '' ? (
                <ErrorMessageComponentInline>Never</ErrorMessageComponentInline>
              ) : (
                <>
                  <SuccessMessageComponentInline>
                    {moment(state.form.lastPublished).format('LLLL')}
                  </SuccessMessageComponentInline>
                </>
              )}
            </li>
            <CopyToClipboard text={`${state.baseRef}${state.slug}`}>
              <li className={'pb-2 cursor-pointer text-lg bg-gray-100 px-3 py-2 mt-2 rounded-lg'}>
                <>
                  Share link <span> (click to copy)</span>:
                  <br />
                  <strong>{`howyou.today`}</strong>
                  {!state.slug ? (
                    <ErrorMessageComponentInline>
                      Missing
                    </ErrorMessageComponentInline>
                  ) : (
                    <>
                      /
                      <SuccessMessageComponentInline>
                        {state.slug}
                      </SuccessMessageComponentInline>
                    </>
                  )}
                </>
              </li>
            </CopyToClipboard>
          </ul>
        </div>
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouPublish
