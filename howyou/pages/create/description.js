import { useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'

const CreateHowYouDescription = (props) => {
  const [state, dispatch] = useContext(Context)

  const changeHandler = (e) => {
    const val = e.currentTarget.value

    if (val) {
      dispatch({
        type: 'SET_DESCRIPTION',
        payload: [val],
      })
    } else {
      dispatch({
        type: 'SET_DESCRIPTION',
        payload: '',
      })
    }
  }

  return (
    <LayoutComponent noFooter clean inline>
      <FormStep
        title={'Description'}
        description={'This is what the visitors will see at the top.'}
        nextStep={'/create/images'}
        previousStep={'/create/slug'}
      >
        <div className='flex flex-row w-full'>
          <textarea
            onChange={changeHandler}
            rows={3}
            className='block w-full px-2 py-1 mb-4 text-xl font-bold text-green-500 border-2 border-gray-300 rounded-lg focus:ring-green-500 focus:border-green-500 bg-gray-50 focus:outline-none'
            placeholder='How are you feeling today?'
            value={state.form.description || null}
          />
        </div>
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouDescription
