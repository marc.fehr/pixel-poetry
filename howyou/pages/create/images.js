import { useContext } from 'react'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import { Context } from '@context/store'

import LayoutComponent from '@components/ui/layout'
import FormStep from '@components/ui/formStep'
import ThumbnailPhoto from '@components/ui/thumbnail'
import ErrorMessageComponent from '@components/ui/errorMessage'
import { SimpleButton } from '@components/ui/button'

const CreateHowYouImages = (props) => {
  const [state, dispatch] = useContext(Context)

  const ThumbnailPhotos = state.form.images?.map((el, i) => {
    return <ThumbnailPhoto key={`photo-${i}`} src={el.src} id={el.id} />
  })

  const templates = {
    sheep: [
      {
        id: 0,
        src: '/images/sheep/sheep-1.png',
      },
      {
        id: 1,
        src: '/images/sheep/sheep-2.png',
      },
      {
        id: 2,
        src: '/images/sheep/sheep-3.png',
      },
      {
        id: 3,
        src: '/images/sheep/sheep-4.png',
      },
      {
        id: 4,
        src: '/images/sheep/sheep-5.png',
      },
      {
        id: 5,
        src: '/images/sheep/sheep-6.png',
      },
      {
        id: 6,
        src: '/images/sheep/sheep-7.png',
      },
      {
        id: 7,
        src: '/images/sheep/sheep-8.png',
      },
      {
        id: 8,
        src: '/images/sheep/sheep-9.png',
      },
    ],
  }

  const setTemplate = (template) => {
    switch (template) {
      case 'sheep':
        dispatch({
          type: 'SET_IMG_TEMPLATE',
          payload: templates.sheep,
        })
        break
      default:
        dispatch({
          type: 'SET_IMG_TEMPLATE',
          payload: templates.sheep,
        })
    }
    console.log(state)
  }

  return (
    <LayoutComponent noFooter clean inline>
      {' '}
      <FormStep
        title={'Upload your moods'}
        description={'You need to add a total of nine images.'}
        nextStep={'/create/publish'}
        previousStep={'/create/description'}
      >
        <SimpleButton className={'mx-auto my-4'} onClick={() => setTemplate('sheep')}>
          <span className={'pr-1'} role={'image'} aria-label={'Sheep emoji'}>
            🐑{' '}
          </span>
          <span>Use random sheep photos</span>
        </SimpleButton>
        <div className='grid w-full grid-cols-3 mb-4 gap-y-1 gap-x-2'>{ThumbnailPhotos}</div>
        {state.slug === '' ||
          (!state.slug && (
            <ErrorMessageComponent className={'mt-3'}>
              Please define a slug before you upload images.
            </ErrorMessageComponent>
          ))}
      </FormStep>
    </LayoutComponent>
  )
}

export const getServerSideProps = withPageAuthRequired()

export default CreateHowYouImages
