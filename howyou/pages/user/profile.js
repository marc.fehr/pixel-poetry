import Image from 'next/image'

import { withPageAuthRequired } from '@auth0/nextjs-auth0'
import WriteToCloudFirestore from '../../components/cloudFirestore/write'
import ReadFromCloudFirestore from '../../components/cloudFirestore/read'
import UploadFile from '../../components/storage/uploadFile'

import LayoutComponent from '../../components/ui/layout'

export default function Profile(props) {
  console.log(props)

  return (
    <LayoutComponent>
      <h1 className={'mb-4'}>Hello, {props.user.name}</h1>
      <p>There is nothing here just yet. But you are logged in, thank you!</p>
    </LayoutComponent>
  )
}

// export const getServerSideProps = withPageAuthRequired()

// You can optionally pass your own `getServerSideProps` function into
// `withPageAuthRequired` and the props will be merged with the `user` prop

export const getServerSideProps = withPageAuthRequired({
  // returnTo: '/foo',
  async getServerSideProps(ctx) {
    const res = await fetch(`https://jsonplaceholder.typicode.com/todos/1`)
    const data = await res.json()

    if (!data) {
      return {
        notFound: true,
      }
    }

    return {
      props: {
        ...data,
      },
    }
  },
})
