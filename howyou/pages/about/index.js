import Image from 'next/image'
import Link from 'next/link'

import LayoutComponent from '../../components/ui/layout'

export default function AboutPage(props) {
  return (
    <LayoutComponent>
      <Image
        src={'/images/logo-square@3x.png'}
        width={150}
        height={150}
        className={'rounded-lg'}
      />
      <h1 className={'mt-2'}>PixelPoetry.dev</h1>
      <p>A project by <Link href={'https://www.marcfehr.ch'}>Marc Fehr</Link></p>
    </LayoutComponent>
  )
}
