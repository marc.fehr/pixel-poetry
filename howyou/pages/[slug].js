import { useContext, useEffect } from 'react'

import { Context } from '@context/store'

import Head from 'next/head'
import { CopyToClipboard } from 'react-copy-to-clipboard'

import LayoutComponent from '../components/ui/layout'
import Gallery from '@components/ui/gallery'

const hri = require('human-readable-ids').hri

const SlugPage = (props) => {
	const [state, dispatch] = useContext(Context)
	const { description, images, slug } = props.data

	useEffect(() => {
		if (slug !== null) {
			if (!localStorage.getItem(slug)) {
				// generate random, unique session ID
				const sessionId = `${hri.random()}-${new Date().getTime()}`
				// ... and add to global state
				dispatch({
					type: 'SET_SESSION',
					payload: sessionId,
				})

				// see if user voted before
				const userVote = JSON.parse(localStorage.getItem(slug))?.userVote || '-1'
				// ... and to to localStorage
				const sessionObject = {
					slug: slug,
					sessionId: sessionId,
					userVote: userVote,
				}
				localStorage.setItem(slug, JSON.stringify(sessionObject))

				// ... then also set to state
				dispatch({
					type: 'SET_USERVOTE',
					payload: userVote,
				})
			} else {
				const sessionData = localStorage.getItem(slug)

				// ... and add to global state
				dispatch({
					type: 'SET_SESSION',
					payload: JSON.parse(sessionData).sessionId,
				})

				// ... then also set to state
				dispatch({
					type: 'SET_USERVOTE',
					payload: JSON.parse(sessionData).userVote,
				})
			}
		}
		if (slug) {
			dispatch({
				type: 'SET_SLUG',
				payload: slug,
			})
		} else {
			dispatch({
				type: 'SET_SLUG',
				payload: '',
			})
		}
		if (images.length > 0) {
			dispatch({
				type: 'SET_IMAGES',
				payload: images,
			})
		} else {
			dispatch({
				type: 'SET_IMAGES',
				payload: [],
			})
		}
	}, [props.data])

	return (
		<LayoutComponent color={state.color} clean noFooter inline>
			<Head>
				<title>HowYou?</title>
				<meta name='description' content='Choose your mood... Which of these images do you identify most with today?' />
				<link rel='icon' href='/favicon.ico' />
				<meta property='og:url' content='https://www.imdb.com/title/tt0117500/' />
				<meta property='og:image' content='/images/teaser.png' />
				<meta name='twitter:title' content='HowYou?' />
				<meta name='twitter:description' content='Choose your mood... Which of these images do you identify most with today?' />
				<meta name='twitter:image' content='/images/teaser.png' />
				<meta name='twitter:card' content='summary_large_image' />
			</Head>

			<h1 className={'bg-gray-800 rounded-lg decoration-clone mb-4 py-1 px-3 text-white text-xl'}>{description}</h1>

			{state.userVote > -1 ? <p className={'mb-4'}>You voted for image {parseInt(state.userVote) + 1}</p> : <p className={'mb-4'}>Vote for one of these images...</p>}
			<div className={'max-w-5xl overflow-hidden rounded-xl'}>
				<Gallery />
			</div>
			<CopyToClipboard text={`${state.baseRef}${slug}`}>
				<>
					<p className={'hidden sm:flex bg-gray-100 rounded-lg decoration-clone mt-5 py-1 px-3 text-gray-800 text-sm cursor-pointer'}>https://howyou.today/{state.slug}</p>
					<p className={'text-gray-500 text-sm'}>(Click to copy)</p>
				</>
			</CopyToClipboard>
		</LayoutComponent>
	)
}

export default SlugPage

export async function getServerSideProps(context) {
	const { slug } = context.query

	const dataRes = await fetch(`${process.env.NODE_ENV === 'production' ? process.env.BASE_URL_PROD : process.env.BASE_URL_DEV}api/data/fetch?slug=${slug}`)
	const data = await dataRes.json()

	if (!data) {
		return {
			notFound: true,
		}
	}

	return {
		props: {
			data,
		},
	}
}
