import LayoutComponent from '@components/ui/layout'

const ErrorPage = (props) => {
  return (
    <LayoutComponent>
      <div>
        <h1>The sadness...</h1>
        <p>This site doesn't exist.</p>
      </div>
    </LayoutComponent>
  )
}

export default ErrorPage
