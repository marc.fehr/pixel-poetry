import NavigationComponent from './navigation'
import FooterComponent from './footer'

const LayoutComponent = ({ clean, children, className, noFooter, inline }) => {
  return (
    <>
      <NavigationComponent clean={clean} />
      <main
        className={`${
          inline ? 'h-auto mt-16 pb-0 sm:pb-20' : 'h-screen min-h-screen-ios'
        } flex justify-center items-center flex-col ${className}`}
      >
        {children}
      </main>
      {!noFooter && <FooterComponent />}
    </>
  )
}

export default LayoutComponent
