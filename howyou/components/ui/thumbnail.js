import { useContext } from 'react'

import Image from 'next/image'
import { Context } from '@context/store'

import UploadFile from '../storage/uploadFile'

const ThumbnailPhoto = (props) => {
  const [state, dispatch] = useContext(Context)
  
  return (
    <div className={`flex items-center justify-center mt-1 ${state.userVote < 0 ? 'cursor-default' : 'cursor-pointer'}`}>
      <span className='inline-block w-auto h-auto overflow-hidden bg-gray-100 rounded rounded-b-none'>
        <div className={'flex justify-center items-center flex-col'}>
          {state.form.images[props.id].src !== '' ? (
            <div className={`m-0 p-0 h-auto flex`}>
              <Image
                src={state.form.images[props.id].src}
                className={'w-full h-full object-cover'}
                width={200}
                height={200}
              />
            </div>
          ) : (
            <svg
              className='w-full h-full text-gray-300 bg-gray-100'
              fill='currentColor'
              viewBox='0 0 24 24'
              width={'200'}
            >
              <path d='M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z' />
            </svg>
          )}
          <div className={`w-full h-full flex justify-center items-center`}>
            <button
              type='button'
              className='relative px-1 py-2 text-sm font-medium leading-4 text-gray-700 shadow-sm focus:outline-none'
            >
              <UploadFile id={props.id} />
            </button>
          </div>
        </div>
      </span>
    </div>
  )
}

export default ThumbnailPhoto
