import { useContext, useEffect, useState } from 'react'

import { Context } from '@context/store'
import Image from 'next/image'
import useSWR from 'swr'

import VoteWrapper from '@components/realtimeDatabase/vote'

const Gallery = (props) => {
  const [state, dispatch] = useContext(Context)
  const [votes, setVotes] = useState([])

  const votesURL = `https://pixelpoetry-2eb90-default-rtdb.europe-west1.firebasedatabase.app/votes/${state.slug}.json`
  const fetcher = (url) => fetch(url).then((res) => res.json())

  const { data, error } = useSWR(votesURL, fetcher, { refreshInterval: 1000 })

  useEffect(() => {
    if (!error && data) {
      const voteValues = Object.values(data)
      setVotes(voteValues)
    } else {
      console.log(error)
    }
  }, [data])

  const Images = state.images.map((img, i) => {
    const userVote = state.userVote

    return (
      <VoteWrapper
        id={img.id}
        className={`relative ${
          state.userVote === '-1' ? 'cursor-pointer' : 'cursor-default'
        }`}
        key={`image-vote-${i}`}
      >
        <div className={`flex`}>
          <Image
            className={`rounded-lg  object-cover`}
            src={img.src}
            width={150}
            height={150}
          />
        </div>
        <span
          className={
            'bg-white px-3 py-1 rounded-full m-1 font-bold absolute top-0 left-0'
          }
        >
          {img.id + 1}
        </span>
        {votes.length >= 0 && (
          <div className={'flex justify-center w-full'}>
            <span
              className={`text-gray-800 border-b-2 border-transparent px-1 mb-1 text-base ${
                img.id === parseInt(userVote)
                  ? `border-gray-800 text-gray-900 opacity-100`
                  : 'opacity-70 border-transparent'
              }`}
            >
              {votes.filter((el) => parseInt(el) === i).length}{' '}
              {votes.filter((el) => parseInt(el) === i).length === 1
                ? 'vote'
                : 'votes'}
              {img.id === parseInt(userVote) && <span> (you)</span>}
            </span>
          </div>
        )}
      </VoteWrapper>
    )
  })

  return (
    <div className={'grid grid-cols-3 gap-x-5 gap-y-2 mx-3 lg:mx-auto'}>
      {Images}
    </div>
  )
}

export default Gallery