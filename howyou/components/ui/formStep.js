import { useContext } from 'react'

import Link from 'next/link'
import { Context } from '@context/store'

import WriteToCloudFirestore from '@components/cloudFirestore/write'

const FormStep = (props) => {
  const [state, dispatch] = useContext(Context)

  const Steps = () => {
    return (
      <>
        <p className={'mb-1 text-xs sm:text-sm texg-gray-700'}>Progress:</p>
        <div className={'mb-4 grid grid-cols-4 items-center'}>
          <Link href={'/create/slug'}>
            <a className={'simple'}>
              <div
                className={`shadow-sm rounded-l-full text-xs sm:text-sm bg-gray-200 py-2 px-1 sm:py-1 sm:px-2 ${
                  !state.slug ? 'bg-gray-100' : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Set slug</p>
              </div>
            </a>
          </Link>
          <Link href={'/create/description'}>
            <a className={'simple'}>
              <div
                className={`shadow-sm text-xs sm:text-sm bg-gray-200 py-2 px-1 sm:py-1 sm:px-2 ${
                  !state.form.description ? 'bg-gray-100' : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Description</p>
              </div>
            </a>
          </Link>
          <Link href={'/create/images'}>
            <a className={'simple'}>
              <div
                className={`shadow-sm text-xs sm:text-sm bg-gray-200 py-2 px-1 sm:py-1 sm:px-2 ${
                  state.form.images?.filter((el) => el.src === '').length > 0 ||
                  !state.form.images
                    ? 'bg-gray-100'
                    : 'bg-green-300'
                } `}
              >
                <p className={'text-center'}>Images</p>
              </div>
            </a>
          </Link>
          <Link href={'/create/publish'}>
            <a className={'simple'}>
              <div
                className={`rounded-r-full shadow-sm text-xs sm:text-sm bg-gray-200 py-2 px-1 sm:py-1 sm:px-2 ${
                  state.form.lastPublished === ''
                    ? 'bg-gray-100'
                    : 'bg-green-300'
                }`}
              >
                <p className={'text-center'}>Publish</p>
              </div>
            </a>
          </Link>
        </div>
      </>
    )
  }

  const Controls = () => {
    return (
      <div className={'flex flex-row justify-between'}>
        <Link href={props.previousStep || '#'}>
          <a
            className={`bg-gray-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-gray-200 ${
              props.previousStep
                ? 'pointer-events-auto'
                : 'pointer-events-none opacity-0'
            }`}
          >
            <button>Back</button>
          </a>
        </Link>
        {!props.publishButton && state.form.lastPublished === '' ? (
          <Link href={props.nextStep || '#'}>
            <a
              className={`bg-gray-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-gray-200 ${
                props.nextStep
                  ? 'pointer-events-auto'
                  : 'pointer-events-none opacity-0'
              }`}
            >
              <button>Next</button>
            </a>
          </Link>
        ) : (
          <WriteToCloudFirestore />
        )}
      </div>
    )
  }

  return (
    <>
      <Steps />
      <div
        className={
          'px-3 py-2 rounded-none shadow-none border-none sm:border-gray-300 sm:shadow-lg border-2 sm:rounded-xl flex w-full h-full flex-col sm:w-3/4 sm:h-1/3 md:w-1/2 lg:h-1/3 lg:w-1/2 justify-betweeen'
        }
      >
        <div className={'flex-1 mt-2'}>
          <div className={'mb-3 rounded-lg'}>
            <h2>{props.title}</h2>
            <h3>{props.description}</h3>
          </div>
          <div className={'flex flex-col items-start justify-center'}>
            {props.children}
          </div>
          {props.error && (
            <p
              className={
                'bg-red-400 text-white rounded-lg text-lg py-2 px-1 sm:py-1 sm:px-2'
              }
            >
              {props.error}
            </p>
          )}
        </div>
        <div>{state.form.lastPublished === '' && <Controls />}</div>
      </div>
    </>
  )
}

export default FormStep
