const ErrorMessageComponent = (props) => {
  return (
    <p className={`py-1 px-2 bg-red-400 text-white flex flex-1 w-full rounded ${props.className}`}>
      {props.children}
    </p>
  )
}

export const ErrorMessageComponentInline = (props) => {
  return (
    <p className={`py-1 px-2 bg-red-400 text-white inline rounded ${props.className}`}>
      {props.children}
    </p>
  )
}

export default ErrorMessageComponent
