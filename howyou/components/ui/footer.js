import Link from 'next/link'

const FooterComponent = (props) => {
  return (
    <footer
      className={`fixed bottom-0 left-0 p-4 flex flex-col justify-center items-center ${props.className}`}
    >
      <Link href='/about'><a data-cy='nav-item' aria-label={'about'}>♥ Read more about PixelPoetry.dev</a></Link>
    </footer>
  )
}

export default FooterComponent
