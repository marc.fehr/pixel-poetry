import Link from 'next/link'

const ButtonComponent = ({ children, href, className }) => {
  return (
    <Link href={href}>
      <button
        className={`py-1 px-3 bg-gray-300 hover:bg-gray-200 rounded-full ${className}`}
      >
        {children}
      </button>
    </Link>
  )
}

export const SimpleButton = ({ children, onClick, className }) => {
  return (
    <button
      onClick={onClick}
      className={`py-1 px-3 bg-gray-300 hover:bg-gray-200 ${className}`}
    >
      {children}
    </button>
  )
}

export default ButtonComponent
