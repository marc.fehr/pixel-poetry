import { useRef, useState, useContext } from 'react'

import firebase from 'firebase/app'
import 'firebase/storage'

import { Context } from '@context/store'

const UploadFile = (props) => {
  const inputEl = useRef(null)
  const [value, setValue] = useState(0)

  const [state, dispatch] = useContext(Context)

  const onButtonClick = () => {
    // `current` points to the mounted file input element
    inputEl.current.click()
  }

  function uploadFile() {
    if (!state.slug) {
      console.log('No slug set')
      return
    }
    const file = inputEl.current.files[0]
    const storageRef = firebase
      .storage()
      .ref(`userUploads/${state.slug}/${file.name}`)
    let task = storageRef.put(file)

    task.on(
      'state_change',
      (snapshot) => {
        setValue((snapshot.bytesTransferred / snapshot.totalBytes) * 100)
      },
      (error) => {
        alert(error)
      },
      () => {
        // alert('Upload to fireabse was successful!')
        // Do something – save file URL to state
        task.snapshot.ref.getDownloadURL().then((downloadURL) => {
          console.log('File available at', downloadURL)
          dispatch({
            type: 'SET_IMG',
            payload: {
              src: downloadURL,
              id: props.id,
            },
          })
        })
      }
    )
  }

  return (
    <div
      className={'flex flex-col justify-center items-center outline-none'}
      onClick={onButtonClick}
    >
      {value > 0 && value < 100 ? (
        <progress max={'100'} className={'w-full h-2'} value={value}></progress>
      ) : (
        <>
          <input
            className={'hidden'}
            type={'file'}
            onChange={uploadFile}
            ref={inputEl}
          />
          <p>Change</p>
        </>
      )}
    </div>
  )
}

export default UploadFile
