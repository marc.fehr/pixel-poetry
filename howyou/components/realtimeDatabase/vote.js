import { useContext } from 'react'

import { Context } from '@context/store'

const VoteWrapper = ({ id, className, children }) => {
  const [state, dispatch] = useContext(Context)

  const voteForImage = async () => {
    const registerVote = () =>
      fetch(
        `/api/vote/register?slug=${state.slug}&sessionId=${state.sessionId}&id=${id}`
      )
    if (JSON.parse(localStorage.getItem(state.slug)).userVote === '-1') {
      registerVote()
        .then((res) => res.json())
        .then((data) => {
          dispatch({
            type: 'SET_USERVOTE',
            payload: data.userVote,
          })

          const currentLocalStorage = JSON.parse(
            localStorage.getItem(state.slug)
          )
          localStorage.setItem(
            state.slug,
            JSON.stringify({ ...currentLocalStorage, userVote: data.userVote })
          )
        })
    } else {
      console.log('You already voted!')
    }
  }

  return (
    <div className={className} onClick={voteForImage}>
      {children}
    </div>
  )
}

export default VoteWrapper
