import { useContext, useState } from 'react'

import firebase from 'firebase/app'
import 'firebase/firestore'

import { Context } from '@context/store'
import { useRouter } from 'next/router'

import { SimpleButton } from '../ui/button'

const WriteToCloudFirestore = (props) => {
  const [state, dispatch] = useContext(Context)
  const router = useRouter()
  const [isPublished, setIsPublished] = useState(false)

  const reRoute = () => {
    const url = `/${state.slug}`
    router.push(url)
  }

  const checkForExistingData = () => {
    try {
      firebase
        .firestore()
        .collection('howyou')
        .where('slug', '==', state.slug)
        .get()
        .then((querySnapshot) => {
          if (querySnapshot.size > 0) {
            console.log(`Slug ${state.slug} was already published!`)
            console.log(snapshot.data())
          } else {
            console.log(`Slug ${state.slug} doesn't exist in database!`)
            sendData()
          }
        })
    } catch (error) {
      console.log(error)
      alert(error)
    }
  }

  const sendData = () => {
    try {
      firebase
        .firestore()
        .collection('howyou')
        .add({
          slug: state.slug,
          userId: state.form.userId,
          description: state.form.description,
          images: state.form.images,
          lastPublished: firebase.firestore.Timestamp.fromDate(new Date()),
        })
        .then((docRef) => {
          console.log('Data was successfully sent to cloud firestore!')
          docRef
            .get()
            .then((doc) => {
              if (doc.exists) {
                console.log('Document data:', doc.data())
                setIsPublished(true)
                dispatch({
                  type: 'SET_PUBLICATION',
                  payload: new Date(
                    (doc.data().lastPublished.seconds +
                      doc.data().lastPublished.nanoseconds * 10 ** -9) *
                      1000
                  ).toString(),
                })
              } else {
                console.log('No such document!')
              }
            })
            .catch((error) => {
              console.log('Error getting document:', error)
            })
        })
    } catch (error) {
      console.log(error)
    }
  }

  console.log(state.form.lastPublished)

  if (state.form.lastPublished === '') {
    return (
      <SimpleButton
        onClick={checkForExistingData}
        className={`bg-green-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-green-200 focus:outline-none`}
      >
        <span>Publish now</span>
      </SimpleButton>
    )
  }

  return (
    <SimpleButton
      onClick={reRoute}
      className={`bg-green-300 w-1/4 h-8 simple flex items-center justify-center rounded hover:bg-green-200 focus:outline-none`}
    >
      <span>Go to quiz</span>
    </SimpleButton>
  )
}

export default WriteToCloudFirestore
